@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    Welcome User!
                    You are logged in the Laravel \(^-^)/!
                    
                </div>
                <div class="title m-b-md">
                     Laravel database relevant links:

                </div>
                <div class="links">
                    <a href="http://localhost/phpmyadmin/sql.php?server=1&db=laravel&table=users&pos=0">Users </a>
                    <a href="http://localhost/phpmyadmin/sql.php?server=1&db=laravel&table=roles&pos=0"> Roles </a>
                    <a href="http://localhost/phpmyadmin/sql.php?server=1&db=laravel&table=permissions&pos=0"> Permissions </a>
                    <a href="http://localhost/phpmyadmin/sql.php?server=1&db=laravel&table=role_has_permissions&pos=0">Role_has_Permissions</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
