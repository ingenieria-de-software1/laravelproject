<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //Permission::create([
        //    'name' => 'usuarios navegando'
        //]);

        DB::table(‘roles’)->insert([
            [‘name’ => ‘Técnico’],
            [‘name’ => ‘Comercial’],
            [‘name’ => ‘Becario’],
            [‘name’ => ‘Invitado’],
            [‘name’ => ‘Administrador’],
        ]);

        DB::table(‘permissions’)->insert([
            [‘name’ => ‘Admin’],
            [‘name’ => ‘Vip’],
            [‘name’ => ‘Free’],
        ]);
    }
}
